#include <stdio.h>

//the ticket price changes by Rs.5 which will change the no of attendants//

int no_of_attendants(int price)
{
int difference,no_of_attendants;
difference = 15-(price);
no_of_attendants = 120+(4*(difference));
     return(no_of_attendants);
}
int temp_revenue(int price, int attendees)
 {
 int temp_revenue;
 temp_revenue = attendees * price ;
 return (temp_revenue); 
 }

int temp_cost(int attendees)
 {
 int temp_cost;
 temp_cost = 500 + (3*attendees);
 return (temp_cost);
 }

int temp_profit(int revenue, int cost)
 {
 int temp_profit;
 temp_profit =  revenue - cost;
 return(temp_profit);
 }

int main()
{
    printf("This table shows the relationship between the Ticket prices and the expected  Profits that can be gained \n");
    printf("Price of the ticket \t Expected Profits \n");
    int price, attendees, revenue, cost, profit, max_profit, max_price;
    
    
    max_profit = 0;
    for (price=5; price<=50; price+=5)
    {
        attendees = no_of_attendants(price);
        revenue = temp_revenue(price,attendees);
        cost = temp_cost(attendees);
        profit = temp_profit(revenue,cost);
        
        printf("   Rs.%d \t              Rs.%d \n",price,profit);
    
if(max_profit < profit)
{
max_profit = profit;   
max_price = price;
}
    }
   
printf("\n The maximum profit, the owner can gain is Rs.%d when he set the ticket price for Rs.%d\n",max_profit,max_price);

 return 0; 
}
    	